FROM alpine:latest
LABEL maintainer="Mausy5043"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            bash \
            tzdata \
            minidlna \
            su-exec


ENV TZ=Europe/Amsterdam

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && echo $TZ > /etc/timezone

# Add config file
COPY minidlna.conf /etc/minidlna.conf
RUN chmod +r /etc/minidlna.conf

# Add entrypoint
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x entrypoint.sh && chown minidlna:minidlna entrypoint.sh

RUN addgroup minidlna users

# VOLUME ["/srv", "var/cache/minidlna"]

EXPOSE 1900/udp
EXPOSE 8200/tcp

ENTRYPOINT ["/entrypoint.sh", "start"]
