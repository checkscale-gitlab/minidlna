#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -f Dockerfile ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

source "${script_dir}"/config.txt

# stop the container
echo -n "Stopping : "
docker stop "${container}"
# throw the container away so we can re-start it
echo -n "Removing : "
docker rm "${container}"
